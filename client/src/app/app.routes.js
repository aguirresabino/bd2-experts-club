(function() {
    'use strict'

    angular.module('ExpertsClub')
        .config(config);

    function config($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: '../views/login.html',
                controller: 'LoginController',
                controllerAs: 'Login'
            })
            .when('/cadastro', {
                templateUrl: '../views/cadastro.html',
                controller: 'LoginController',
                controllerAs: 'Login',
                //authorize: true
            })
            .when('/experts-club', {
                templateUrl: '../views/topicos.html',
                controller: 'TopicoController',
                controllerAs: 'Topico',
                //authorize: true
            })
            .when('/topicos/:categoria', {
                templateUrl: '../views/topicos.html',
                controller: 'TopicoController',
                controllerAs: 'Topico',
            })
            .when('/topico/:id', {
                templateUrl: '../views/topico.detail.html',
                controller: 'TopicoDetailController',
                controllerAs: 'TopicoDetail'
            })
            .when('/cadastrar-topico', {
                templateUrl: '../views/cadastrar.topico.html',
                controller: 'TopicoController',
                controllerAs: 'Topico'
            })
            .when('/buscar', {
                templateUrl: '../views/busca.html',
                controller: 'TopicoController',
                controllerAs: 'Topico'
            })
            .otherwise({
                redirectTo: '/login'
            })
        // $locationProvider.html5Mode(true);
    }
    config.$inject = ['$routeProvider']
})();