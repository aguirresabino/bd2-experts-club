(function() {
    'use strict';

    angular.module('InicioController', ['UsuarioService', 'TopicoService'])
        .controller('InicioController', ['UsuarioService', 'TopicoService', '$rootScope', InicioController]);

    function InicioController(UsuarioService, TopicoService, $rootScope) {
        let vm = this;

        if (angular.fromJson(localStorage.usuario)) {
            $rootScope.logado = true;
        }

        vm.sair = () => {
            localStorage.clear();
            $rootScope.logado = false;
        }

        let allCategorias = function() {
            TopicoService.readCategorias()
                .then((success) => {
                    vm.categorias = success.data;
                })
                .catch((error) => {
                    console.log('error', error);
                });
        }();
    }
})();