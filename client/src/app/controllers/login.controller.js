(function() {
    'use strict';

    angular.module('LoginController', ['UsuarioService'])
        .controller('LoginController', ['UsuarioService', 'TopicoService', '$location', '$rootScope', LoginController]);

    function LoginController(UsuarioService, TopicoService, $location, $rootScope, $scope) {
        let vm = this;

        vm.login = (login) => {
            UsuarioService.login(login.email, login.senha)
                .then((success) => {
                    console.log(success.data);
                    localStorage.usuario = angular.toJson(success.data);
                    $location.path('/experts-club');
                })
                .catch((error) => {
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Verifique se os dados informados estão corretos e tente novamente.',
                    });
                    console.log('error', error);
                });
        }

        vm.create = (usuario) => {
            UsuarioService.create(usuario)
                .then((success) => {
                    swal({
                        type: 'success',
                        title: `Usuário(a) ${usuario.nome} foi cadastrado(a)!`,
                        text: 'O cadastro foi realizado!.',
                    });
                })
                .catch((error) => {
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Verifique se os dados informados estão corretos e tente novamente.',
                    });
                    console.log('error', error);
                })
        }

        if (angular.fromJson(localStorage.usuario)) {
            $rootScope.logado = true;
        }

        vm.sair = () => {
            localStorage.clear();
            $rootScope.logado = false;
        }

        let allCategorias = function() {
            TopicoService.readCategorias()
                .then((success) => {
                    vm.categorias = success.data;
                })
                .catch((error) => {
                    console.log('error', error);
                });
        }();
    }
})();