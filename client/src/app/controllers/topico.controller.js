(function() {
    'use strict';

    angular.module('TopicoController', ['UsuarioService', 'TopicoService'])
        .controller('TopicoController', ['UsuarioService', 'TopicoService', '$routeParams', TopicoController]);

    function TopicoController(UsuarioService, TopicoService, $routeParams) {
        let vm = this;

        let usuario = angular.fromJson(localStorage.usuario);

        let categoria = $routeParams.categoria;

        vm.create = (topico) => {
            topico.usuario = usuario.id;
            TopicoService.create(topico)
                .then((success) => {
                    swal({
                        type: 'success',
                        title: 'Tópico cadastrado!',
                        confirmButtonText: 'Ok'
                    }).then(function(result) {
                        location.reload();
                    });
                })
                .catch((error) => {
                    console.log('error', error);
                })
        }

        let allCategorias = function() {
            TopicoService.readCategorias()
                .then((success) => {
                    vm.categorias = success.data;

                    //Atualizando select do materialize
                    setTimeout(function() {
                        $('select').material_select();
                    }, 100);
                })
                .catch((error) => {
                    console.log('error', error);
                });
        }();

        function buscar(busca) {
            TopicoService.buscar(busca)
                .then((success) => {
                    vm.resultados = success.data;
                })
                .catch((error) => {
                    console.log('error', error);
                })
        }

        $(document).on('keydown', function(event) {

            if (event.keyCode === 13) {

                let busca = $("#search").val();
                buscar(busca);

            }

        });

        let topicos = function() {
            TopicoService.read()
                .then((success) => {
                    vm.topicos = success.data;
                })
                .catch((error) => {
                    console.log('error', error);
                })
        };

        let topicosByCategoria = (categoria) => {
            TopicoService.readByCategoria(categoria)
                .then((success) => {
                    vm.topicos = success.data;
                })
                .catch((error) => {
                    console.log('error', error);
                })
        }

        if (categoria) {
            topicosByCategoria(categoria);
        } else {
            topicos();
        }

    }
})();