(function() {
    'use strict';

    angular.module('TopicoDetailController', ['UsuarioService', 'TopicoService'])
        .controller('TopicoDetailController', ['UsuarioService', 'TopicoService', '$routeParams', TopicoDetailController]);

    function TopicoDetailController(UsuarioService, TopicoService, $routeParams) {
        let vm = this;

        let usuario = angular.fromJson(localStorage.usuario);

        let id = $routeParams.id;

        let categoria;

        let topicosById = function() {
            TopicoService.readById(id)
                .then((success) => {
                    vm.topico = success.data;
                    categoria = vm.topico.categoria;

                    console.log('aaaaa', categoria);
                })
                .catch((error) => {
                    console.log('error', error);
                })
        }();

        

        let sugestao = function() {
            TopicoService.sugestao(categoria)
                .then((success) => {
                    console.log(success.data);
                    vm.topicos = success.data;
                })
                .catch((error) => {
                    console.log('error', error);
                })
        };

        setTimeout(function(args) {
            sugestao();
        },1000)

        let respostas = function() {
            TopicoService.readRespostasTopico(id)
                .then((success) => {
                    vm.respostas = success.data.respostas;
                })
                .catch((error) => {
                    console.log('error', error);
                })
        }();

        vm.responder = (resposta, idTopico) => {
            resposta = { usuario: usuario.id, conteudo: resposta };
            TopicoService.createRespostasTopico(angular.toJson(resposta), idTopico, usuario.id)
                .then((success) => {
                    swal({
                        type: 'success',
                        title: 'Resposta enviada!',
                        confirmButtonText: 'Ok'
                    }).then(function(result) {
                        location.reload();
                    });
                })
                .catch((error) => {
                    console.log('error', error);
                })
        }
    }
})();