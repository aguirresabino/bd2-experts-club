(function() {

    'use strict';

    angular.module('UsuarioController', ['UsuarioService'])
        .controller('UsuarioController', ['UsuarioService', UsuarioController]);

    function UsuarioController(UsuarioService) {
        let vm = this;

        vm.perfil = angular.fromJson(localStorage.usuario);

        vm.create = function(adm) {
            let novoAdm = {};

            UsuarioService.create(novoAdm)
                .then(function(success) {
                    swal({
                        type: 'success',
                        title: 'Cadastro realizado!',
                        text: 'O administrador ' + novoAdm.nome + ' foi realizado!',
                    });
                    location.reload();
                })
                .catch(function(error) {
                    swal({
                        type: 'error',
                        title: 'O cadastro não foi realizado!',
                        text: 'Informe todos os dados corretamente.',
                    });
                    console.log('Error adm cadastrado', error);
                })
        };

        vm.update = function(dados) {

            UsuarioService.update(perfil)
                .then(function(success) {

                    updateCacheUsuarioLogado(perfil);
                    readSingle();

                    swal({
                        type: 'success',
                        title: 'Dados alterados!'
                    });
                })
                .catch(function(error) {
                    swal({
                        type: 'error',
                        title: 'Os dados não foram modificados!',
                        text: 'Informe todos os dados corretamente.',
                    });
                    console.log('Perfil não atualizado', error);
                })
        };
    }
})();