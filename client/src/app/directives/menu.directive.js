(function() {
    "use strict";

    angular.module('Menu', [])
        .directive("menu", MenuDiretiva);

    function MenuDiretiva() {
        return {
            templateUrl: "../../views/menu.html",
            controller: "LoginController",
            controllerAs: "Login"
        };
    }
})();