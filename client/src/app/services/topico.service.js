(function() {
    'use strict';

    angular.module("TopicoService", [])
        .service("TopicoService", TopicoService);

    function TopicoService($http) {
        const urlBase = 'http://localhost:3000/experts-club-api';
        const methods = {
            GET: 'GET',
            POST: 'POST',
            PUT: 'PUT',
            DELETE: 'DELETE'
        }

        this.create = (topico) => {
            let path = 'topicos';

            let request = {
                url: urlBase + "/" + path,
                method: methods.POST,
                data: topico,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.read = () => {
            let path = 'topicos';

            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.readById = (id) => {
            let path = `topicos/${id}`;

            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.sugestao = (categoria) => {
            let path = `sugestao/${categoria}`;
            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.readByCategoria = (categoria) => {
            let path = `topicos/categoria/${categoria}`;

            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.buscar = (busca) => {
            let path = `busca`;

            console.log('busca aqui', busca);

            let request = {
                url: urlBase + "/" + path,
                method: methods.POST,
                data: {busca}
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.createRespostasTopico = (resposta, idTopico, idUsuario) => {
            let path = `topicos/resposta/${idTopico}`;

            let request = {
                url: urlBase + "/" + path,
                method: methods.POST,
                data: resposta
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.readRespostasTopico = (id) => {
            let path = `topicos/resposta/${id}`;

            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.readCategorias = () => {
            let path = 'categorias';

            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }
    }
    TopicoService.$inject = ['$http']
})();