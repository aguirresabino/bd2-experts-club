(function() {
    'use strict';

    angular.module("UsuarioService", [])
        .service("UsuarioService", UsuarioService);

    function UsuarioService($http) {
        const urlBase = 'http://localhost:3000/experts-club-api';
        const methods = {
            GET: 'GET',
            POST: 'POST',
            PUT: 'PUT',
            DELETE: 'DELETE'
        }

        this.login = (email, senha) => {
            let path = `login/${email}/${senha}`;
            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }

            return $http(request);
        }

        this.create = (usuario) => {
            let path = 'usuarios';

            let request = {
                url: urlBase + "/" + path,
                method: methods.POST,
                data: usuario,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.readByEmail = (email) => {
            let path = `usuarios/${email}`;

            let request = {
                url: urlBase + "/" + path,
                method: methods.GET,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.update = (usuario) => {
            let path = `usuarios/${email}`;

            let request = {
                url: urlBase + "/" + path,
                method: methods.PUT,
                data: usuario,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

        this.delete = (usuario) => {
            let path = `usuarios/${email}`;

            let request = {
                url: urlBase + "/" + path,
                method: methods.DELETE,
                data: usuario,
                //headers: { 'Authorization': 'Bearer ' + localStorage.token }
            }

            return $http(request);
        }

    }
    UsuarioService.$inject = ['$http']
})();