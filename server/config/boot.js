module.exports = (api) => {
    api.listen(api.get('port'), () => {
        console.log(`Servidor rodando na porta: ${api.get('port')}`);
    })
}