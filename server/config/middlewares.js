//Implementa toda lógica dos middlewares e configurações do express

let logger = require('morgan');
let bodyParser = require('body-parser');
let cors = require('cors');
let helmet = require('helmet');

module.exports = (api) => {
	let port = parseInt(process.env.PORT, 10) || 3000;    
    api.set('port', port);
    api.set('json spaces', 4);
	
    api.use(cors({
        origin: ["http://localhost:3001"],
        methods: ["GET", "POST", "PUT", "DELETE", "OPTIONS"],
        allowedHeaders: ['Content-Type', 'Authorization']
    }));
    api.use(logger('dev'));
    api.use(helmet());
    api.use(bodyParser.json());
    api.use(bodyParser.urlencoded({ extended: true }));
};