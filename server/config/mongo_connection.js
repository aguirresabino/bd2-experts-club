const mongoose = require('mongoose');
let single_connection;
let url = 'mongodb://localhost/experts-club';

module.exports = (api) => {
    if (!single_connection) {
        single_connection = mongoose.connect(url);
    }

    return single_connection;
}