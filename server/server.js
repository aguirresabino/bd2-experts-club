let express = require('express');
let consign = require('consign');

let api = express();

let router = express.Router();

router.use((req, res, next) => {
	console.log("Algo acontecendo");
	next();
});

router.get('/', (req, res) => {
    res.json({ message: 'Olá, você está na api do experts club.' });
});

api.use('/experts-club-api', router);  //Rota raiz


consign()
    .include('./config/middlewares.js')
    .then('./src/usuario/index.js')
    .then('./src/usuario/usuario.routes.js')
    .then('./config/mongo_connection.js')
    .then('./src/topico/topico.model.js')
    .then('./src/topico/topico.controller.js')
    .then('./src/topico/topico.routes.js')
    .then('./config/boot.js')
    .into(api); //indica que os módulos que estão sendo incluídos podem receber um parâmetro na função