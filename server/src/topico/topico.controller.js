'use strict';

function responseCallback(error, success, res) {
    if (error) {
        return res.status(500).json({ 'error message': error })
    }
    res.status(200).json(success);
}

const Topico = require('./topico.model');

module.exports = {

    create(req, res) {
        Topico.create(req.body, (error, success) => { responseCallback(error, success, res) });
    },

    //lista todos os tópicos
    read(req, res) {
        Topico.find((error, success) => { responseCallback(error, success, res) });
    },

    //listar topico pelo id
    readByTopico(req, res) {
        const id = {_id:req.params.id};
        Topico.findById(id, (error, success) => { responseCallback(error, success, res) });
    },

    //atualizar topico pelo id
    update(req, res){
        const id = {_id:req.params.id};
    	Topico.findOneAndUpdate({"_id" : id}, req.body, (error, success) => { responseCallback(error, success, res)})
    },

    //deletar Topico
    delete(req, res){
        const id = {_id:req.params.id};
        Topico.findByIdAndRemove(id);
    }

}