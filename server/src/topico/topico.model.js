const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const schema = mongoose.Schema;

const RespostaSchema = new schema({
    usuario: { type: Number, required: true },
    conteudo: { type: String, required: true },
    data: { type: Date, default: Date.now },
});

const TopicoSchema = new schema({
    usuario: { type: Number, required: true },
    categoria: { type: Number, required: true },
    titulo: { type: String, required: true },
    conteudo: { type: String, required: true },
    respostas: [RespostaSchema],
    data: { type: Date, default: Date.now}
}, {usePushEach: true});

TopicoSchema.index({titulo: 'text', conteudo: 'text'});

module.exports = mongoose.model('topico', TopicoSchema);