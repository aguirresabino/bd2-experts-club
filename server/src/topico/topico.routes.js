'use strict';

module.exports = (api) => {
    let TopicosController = require('./topico.controller');

    const urlBase = '/experts-club-api';

    api.route(`${urlBase}/topicos`)
        .post((req, res) => {
            TopicosController.create(req, res);
        })
        .get((req, res) => {
            TopicosController.read(req, res);
        });

    api.route(`${urlBase}/topicos/:id`)
        .get((req, res) => {
            TopicosController.readById(req, res);
        })
        .put((req, res) => {
            TopicosController.update(req, res);
        })
        .delete((req, res) => {
            TopicosController.delete(req, res);
        });

    api.route(`${urlBase}/busca`)
        .post((req, res) => {
            TopicosController.readByText(req, res);
        });

    api.route(`${urlBase}/topicos/categoria/:categoria`)
        .get((req, res) => {
            TopicosController.readByCategoria(req, res);
        });

    api.route(`${urlBase}/sugestao/:categoria`)
        .get((req, res) => {
            TopicosController.readLimit(req, res);
        });        

    api.route(`${urlBase}/topicos/resposta/:id`)
        .post((req, res) => {
            TopicosController.addResposta(req, res);
        })
        .get((req, res) => {
            TopicosController.respostasByTopico(req, res);
        });

    api.route(`${urlBase}/categorias`)
        .get((req, res) => {
            const categorias = [{id: 0, nome: 'Programação'},
                                  {id: 1, nome: 'Front-end'},
                                  {id: 2, nome: 'Infraestrutura'},
                                  {id: 3, nome: 'Mobile'},
                                  {id: 4, nome: 'Design & UX'}];
            res.status(200).json(categorias);
        })
}