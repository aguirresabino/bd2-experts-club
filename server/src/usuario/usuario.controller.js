let Usuario = require('.').usuario;
var redis = require("redis");
const client = redis.createClient();

client.on("error", function (err) {
    console.log("Error " + err);
});

module.exports = {
    login(req, res) {
        return Usuario
            .find({
                where: {
                    email: req.params.email,
                    senha: req.params.senha
                }
                
            })
            .then(success => {
                if (!success) {
                    return res.satatus(404).send({
                        message: "Usuário não encontrado",
                    });
                }
                return res.status(200).send(success);
            })
            .catch(error => res.status(400).send(error));
    },
    create(req, res) {
        client.set(req.params.email + "Usuario criado", "Usuario criado");
        return Usuario
            .create({
                nome: req.body.nome,
                email: req.body.email,
                senha: req.body.senha
            })
            .then(success => res.status(201).send(success))
            .catch(error => res.status(400).send(error));
    },
    read(req, res) {
        return Usuario
            .findOne({
                where: { email: req.params.email }
            })
            .then(success => {
                if (!success) {
                    return res.status(404).send({
                        message: 'Usuario Not Found',
                    });
                }
                return res.status(200).send(success);
            })
            .catch(error => res.status(400).send(error));
    },
    update(req, res) {
        client.set(req.params.email + "Usuario atualizado", "atulizado");
        return Usuario
            .findOne({
                where: { email: req.params.email }
            })
            .then(success => {
                if (!success) {
                    return res.status(404).send({
                        message: 'Usuario Not Found',
                    });
                }
                return success
                    .update({
                        nome: req.body.nome || success.nome,
                        email: req.body.email || success.email,
                		senha: req.body.senha || success.senha
                    })
                    .then(() => res.status(200).send(success))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
    delete(req, res) {
        client.del(req.params.email);
    	return Usuario
    		 .findOne({
                where: { email: req.params.email }
            })
    		 .then(success => {
    		 	if(!success){
    		 		return res.status(404).send({
                        message: 'Usuario Not Found',
                    });
    		 	}
    		 	return success
    		 		.destroy()
    		 		.then(() => res.status(204).send())
        			.catch(error => res.status(400).send(error));
    		 })
    		 .catch(error => res.status(400).send(error));
    }
}