'use strict';
module.exports = (sequelize, DataTypes) => {
    let usuario = sequelize.define('usuario', {
        nome: DataTypes.STRING,
        email: DataTypes.STRING,
        senha: DataTypes.STRING
    }, {});
    usuario.associate = function(models) {
        // associations can be defined here
    };
    return usuario;
};