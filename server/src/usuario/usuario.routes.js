'use strict';

let userCtrl = require('./usuario.controller');

module.exports = (api) => {
    const urlBase = '/experts-club-api';

    api.route(`${urlBase}/login/:email/:senha`)
    	.get((req, res) => {
    		userCtrl.login(req, res);
    	});

    api.route(`${urlBase}/usuarios`)
    	.all((req, res, next) => {

    		//garantindo que não o objeto req.body só terá os dados necessários
    		//evitando também injeção post
    		console.log(req.body, 'ANTES');
    		let user = {};
    		user.nome = req.body.nome;
    		user.email = req.body.email;
    		user.senha = req.body.senha;

    		req.body = user;
    		console.log(req.body, 'DEPOIS');
    		next();
    	})
        .post((req, res) => {
            userCtrl.create(req, res);
        });

    api.route(`${urlBase}/usuarios/:email`)
        .all((req, res, next) => {
        	
        	let user = {};
    		user.nome = req.body.nome;
    		user.email = req.body.email;
    		user.senha = req.body.senha;

    		req.body = user;
            next();
        })
        .get((req, res) => {
            userCtrl.read(req, res);
        })
        .put((req, res) => {
            userCtrl.update(req, res);
        })
        .delete((req, res) => {
            userCtrl.delete(req, res);
        });
}